package main

import (
	"crud-go/controllers"
	"crud-go/initializers"

	"github.com/gin-gonic/gin"
)

     func init()  {
     initializers.LoadEnvVariables()
     initializers.ConnectToDB()

     }
func main() {
  r := gin.Default()
  r.POST("/posts", controllers.ShoppingListCreate)
  r.GET("/posts", controllers.ShoppingListIndex)
  r.GET("/posts/:id", controllers.ShoppingListShow)
  r.GET("/hitung", controllers.HitungItem)
  r.Run() // listen and serve on 0.0.0.0:8080 (for windows "localhost:8080")
}