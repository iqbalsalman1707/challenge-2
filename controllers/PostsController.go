package controllers

import (
	"crud-go/initializers"
	"crud-go/models"

	"github.com/gin-gonic/gin"
)

func ShoppingListCreate (c *gin.Context) {
    
	//create data ke database
    var shoppinglist struct{
		Nmprduk string
		Iduser int
		Qty int
		Price int
		Total int
	}
	c.Bind(&shoppinglist)
	post := models.Posts{Nmprduk: shoppinglist.Nmprduk,
		    Iduser: uint(shoppinglist.Iduser), 
		    Price:uint(shoppinglist.Price),Qty: uint(shoppinglist.Qty),
			Total: uint(shoppinglist.Qty)*uint(shoppinglist.Price) }
    result := initializers.DB.Create(&post)

	if result.Error !=nil{
		c.Status(400)
		return
	}

	c.JSON(200, gin.H{
		"post": post,

	})

}

func ShoppingListIndex (c *gin.Context)  {
	var posts []models.Posts
	initializers.DB.Find(&posts)

	c.JSON(200, gin.H{
		"posts": posts,

	})
}
func ShoppingListShow (c *gin.Context)  {
	id:= c.Param("id")
	var posts models.Posts
	initializers.DB.First(&posts,id)

	c.JSON(200, gin.H{
		"posts": posts,

	})
}

func HitungItem(c *gin.Context) {
	var result []struct {
		IdUser      int64   `json:"iduser" gorm:"column:iduser"`
		JumlahItem  int64   `json:"jumlah_item"`
		TotalBelanja float64 `json:"total_belanja"`
	}

	// GROUP BY query menggunakan ORM GORM
	initializers.DB.Table("posts").
		Select("iduser, COUNT(nmprduk) as jumlah_item, sum(total) as total_belanja").
		Group("iduser").
		Order("iduser ASC").
		Scan(&result)

	c.JSON(200, gin.H{
		"result": result,
	})
}
func DeletePost (c *gin.Context)  {
	id:= c.Param("id")
	var posts models.Posts
	initializers.DB.Delete(&posts,id)

	c.Status(200)
}
 func ShoppingListUpdate(c *gin.Context)  {
	id:= c.Param("id")
	var shoppinglistUpdate struct{
		Nmprduk string
		Iduser int
		Qty int
		Price int
		Total int
	}
	c.Bind(&shoppinglistUpdate)

	var post models.Posts
	initializers.DB.First(&post,id)
	initializers.DB.Model(&post).Updates(models.Posts{Nmprduk: shoppinglistUpdate.Nmprduk,
		                                Iduser: uint(shoppinglistUpdate.Iduser), 
		                                Price:uint(shoppinglistUpdate.Price),Qty: uint(shoppinglistUpdate.Qty),
		                                Total: uint(shoppinglistUpdate.Qty)*uint(shoppinglistUpdate.Price)})

    c.JSON(200, gin.H{
	"post": post,
									
    })
									
	
 }